import os, sys

inFilePath = sys.argv[1]
sampleIDs = sys.argv[2].split(",")
outFilePath = sys.argv[3]

inFile = open(inFilePath)
outFile = open(outFilePath, 'w')

for line in inFile:
    if line.startswith("##"):
        outFile.write(line)
        continue

    if line.startswith("#"):
        headerItems = line.rstrip().split("\t")
        itemIndices = range(9) + [headerItems.index(sampleID) for sampleID in sampleIDs]

    lineItems = line.rstrip().split("\t")
    lineItems = [lineItems[i] for i in itemIndices]
    outFile.write("\t".join(lineItems) + "\n")

outFile.close()
inFile.close()
