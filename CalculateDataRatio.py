import os, sys

normalBamFilePath = sys.argv[1]
tumorBamFilePath = sys.argv[2]

normalSize = float(os.stat(normalBamFilePath).st_size)
tumorSize = float(os.stat(tumorBamFilePath).st_size)

if normalSize > tumorSize:
    print 1.00
else:
    print "%.2f" % (normalSize / tumorSize)
