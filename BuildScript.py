import os, sys, glob

sampleID = sys.argv[1]
fastqDirPath = sys.argv[2]
speedSeqFilePath = sys.argv[3]
referenceGenomeFilePath = sys.argv[4]
tmpDirPath = sys.argv[5] + "/" + sampleID
numThreads = sys.argv[6]
memory = sys.argv[7]
outBAMDirPath = sys.argv[8]
outScriptDirPath = sys.argv[9]

if not os.path.exists(tmpDirPath):
    os.makedirs(tmpDirPath)

command = "#!/bin/bash\n\n"
command += "#SBATCH --time=48:00:00\n"
#command += "#SBATCH --ntasks=14\n"
command += "#SBATCH --ntasks=10\n"
command += "#SBATCH --nodes=1\n"
command += "#SBATCH --mem-per-cpu=4000M\n\n"

command += "set -o errexit\n\n"

command += "%s align -o %s/%s.aligned -M %s -t %s -R '@RG\\tID:%s\\tSM:%s\\tLB:lib1' -T %s %s " % (speedSeqFilePath, outBAMDirPath, sampleID, memory, numThreads, sampleID, sampleID, tmpDirPath, referenceGenomeFilePath)

inFilePaths = glob.glob(fastqDirPath + "/" + sampleID + "*_1.fastq.dsrc")

if len(inFilePaths) == 0:
    print "No input files are available for %s" % sampleID
    exit(0)

in1 = "<(./dsrc d -s " + inFilePaths[0]
in2 = "<(./dsrc d -s " + inFilePaths[0].replace("_1.fastq.dsrc", "_2.fastq.dsrc")

if len(inFilePaths) > 1:
    for f1 in inFilePaths[1:]:
        in1 += " && ./dsrc d -s " + f1
        in2 += " && ./dsrc d -s " + f1.replace("_1.fastq.dsrc", "_2.fastq.dsrc")

in1 += ") "
in2 += ") "

command += in1 + in2 + "\n"

outScriptFile = open(outScriptDirPath + "/" + sampleID, 'w')
outScriptFile.write(command)
outScriptFile.close()

#for inFilePath in inFilePaths:
#    print inFilePath

#print "Created script for %s" % sampleID
