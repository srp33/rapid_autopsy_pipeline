import os, sys

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

outFile = open(outFilePath, 'w')

for line in file(inFilePath):
    if line.startswith("#"):
        outFile.write(line)
        continue

    lineItems = line.rstrip().split("\t")
    genotype = lineItems[9].split(":")[0]

    if genotype == "0/0" or genotype == "":
        continue

    outFile.write(line)

outFile.close()
