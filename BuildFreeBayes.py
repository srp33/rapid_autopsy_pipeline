import os, sys

bamFilePath = sys.argv[1]
scriptOutPrefix = sys.argv[2]
hours = sys.argv[3]
megabytes = sys.argv[4]
regionFile = sys.argv[5]
tmpOutPrefix = sys.argv[6]
outPrefix = sys.argv[7]

regionDict = {}
regionCount = 1
for line in file(regionFile):
    lineItems = line.rstrip().split("\t")
    regionDict["Region%i" % regionCount] = lineItems[0]
    regionCount += 1

for regionName in regionDict:
    region = regionDict[regionName]
    tmpVcfFilePath = tmpOutPrefix + "__" + regionName + ".vcf"
    vcfFilePath = outPrefix + "__" + regionName + ".vcf"
    scriptFilePath = scriptOutPrefix + "__" + regionName

    if os.path.exists(vcfFilePath):
        print "Not creating script file for %s because VCF already exists." % regionName
        continue

    outFile = open(scriptFilePath, 'w')

    outFile.write("#!/bin/bash\n\n")
    outFile.write("#SBATCH --time=" + hours + ":00:00\n")
    outFile.write("#SBATCH --ntasks=1\n")
    outFile.write("#SBATCH --nodes=1\n")
    outFile.write("#SBATCH --mem-per-cpu=" + megabytes + "M\n\n")
    outFile.write("set -o errexit\n\n")
    outFile.write("Software/freebayes/bin/freebayes -f Genome/genome.fa -L " + bamFilePath + " -r " + region + " --pooled-continuous --pooled-discrete --report-genotype-likelihood-max --allele-balance-priors-off --genotype-qualities --min-repeat-entropy 1 --min-alternate-fraction 0.05 --min-alternate-count 4 | Software/freebayes/vcflib/bin/vcffilter -f \"QUAL > 5\" > " + tmpVcfFilePath + "\n")
    outFile.write("mv " + tmpVcfFilePath + " " + vcfFilePath + "\n")

    outFile.close()
