# Combines Freebayes VCFs from each chromosome into one VCF

import sys
import os
import re
import glob

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)

inFilePattern = sys.argv[1]
outFile = sys.argv[2]

# get list of files
vcfList = natural_sort([vcf for vcf in glob.glob(inFilePattern)])

# add to the output file from each input file
outputFile = open(outFile, "w")
firstFile = True

for thisVcf in vcfList:
	print "Processing %s" % thisVcf
	vcfFile = open(thisVcf)

	for line in vcfFile:
		if firstFile: # if this is the first file, write the line to output file whether or not it's a header line
			outputFile.write(line)
		else: # if this is not the first file, only write the line if it's not a header (we only need one header, which comes from the first file)
			if not line.startswith("#"):
				outputFile.write(line)

	firstFile = False

	vcfFile.close()

outputFile.close()
