import os, sys

inFilePath = sys.argv[1]
inHeader = sys.argv[2]
outHeader = sys.argv[3]
outFilePath = sys.argv[4]

outFile = open(outFilePath, 'w')

for line in file(inFilePath):
    if line.startswith("#CHROM"):
        lineItems = line.rstrip().split("\t")
        index = lineItems.index(inHeader)
        lineItems[index] = outHeader
        line = "\t".join(lineItems) + "\n"

    outFile.write(line)

outFile.close()
